/*test AB-testing*/
function PageB () {

/*changement couleur logo*/
let logo02=document.querySelector("div.header-logo a img");
logo02.src="./img/logo-default.png";

/*annulation sidebar*/
let sidebar = document.querySelector("aside.sidebar");
sidebar.style.display="none";

/*changement images carousel 1*/
let imageCar01= document.querySelector("div.owl-carousel div:nth-child(1) img");
let imageCar02 = document.querySelector("div.owl-carousel div:nth-child(2) img");
let imageCar03 = document.querySelector("div.owl-carousel div:nth-child(3) img" );
imageCar01.src="./img/products/sacbleu.jpg";
imageCar02.src="./img/products/sacbleu02.jpg";
imageCar03.src="./img/products/sacbleu03.jpg";

/*dimensions totale largeur image + texte*/
let bloc01 = document.querySelector("div.main div.container  div.row div.col-lg-9");
bloc01.className = "col-lg-12";

/*replace bouton add to cartd bloc01*/
let boutAddcart=document.querySelector("form.cart");
boutAddcart.style.display="flex";
boutAddcart.style.flexDirection="column";
boutAddcart.style.alignItems="center";
boutAddcart.style.marginTop="120px";

/*agrandir taille police bouton add to cart*/
let boutAddcartButton=document.querySelector("form.cart button.btn");
boutAddcartButton.style.fontSize="26px"

/*déplacer .product-meta avant form.cart*/
let descrSac = document.querySelector("div.summary");
let cat = document.querySelector("div.product-meta");
descrSac.insertBefore(cat,boutAddcart);

/*insérer images related products*/
let appPhot = document.querySelector("div.masonry-loader div.product-thumb-info-list div:nth-child(1) span img");
let sacGolf = document.querySelector("div.masonry-loader > div.row > div.product:nth-child(2) > span > a:nth-child(2)> span img");
let workout = document.querySelector("div.masonry-loader > div.row > div.product:nth-child(3) > span > a:nth-child(2)> span img");
let bagLux = document.querySelector("div.masonry-loader > div.row > div.product:nth-child(4) > span > a:nth-child(2)> span img");
appPhot.src="./img/products/appPhoto.jpg";
sacGolf.src="./img/products/sacGolf.jpg";
workout.src="./img/products/workout.jpg";
bagLux.src="./img/products/bagLux.jpg";

/*dimension images bas ici hauteur*/
let photosBas = document.querySelectorAll("div.masonry-loader > div.row > div.product > span > a:nth-child(2)> span img");
photosBas.forEach((photoBH) => {
    photoBH.style.height="256px"
});

/*annul bordures arrondies bouton subscribe*/
let bordButton = document.querySelector("#newsletterForm div.input-group span.input-group-append button.btn");
bordButton.style.borderRadius = "0px";

/*ajout icône |¦⁞ bouton subsribe*/
//création de la div avec classe iconeBut
let imgIcon = document.createElement('img');
imgIcon.className= 'iconBut';
//appel de l'image url
imgIcon.src="./img/icons/iconBtn.png"
console.log(imgIcon);
document.querySelector('#newsletterForm div.input-group input.form-control').appendChild(imgIcon);//img enfant de classe 
let sp2 = document.querySelector('#newsletterForm div.input-group input.form-control');
let parentImg = sp2.parentNode;
parentImg.insertBefore(imgIcon,sp2.nextSibling);
imgIcon.style.width = "22px";
imgIcon.style.height = "120px";

}

PageB();











